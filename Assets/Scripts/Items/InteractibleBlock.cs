﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interactable blocks abstract class. Parent of all block kinds.
/// </summary>
public abstract class InteractibleBlock : MonoBehaviour
{
    public enum Bonus { Nothing, Empty, Mushroom, Coins, Live, Star, Flower }
    public Bonus bonus;

    public Vector3 yMovement;
    public float movementSpeed;
    public bool visible = true;

    private Collider2D blockCollider;
    protected SpriteRenderer spriteRenderer;

    private Vector3 startPos;
    private Vector3 desiredPos1;

    //private int scoreEffectIndex;

    protected virtual void Start()
    {
        blockCollider = GetComponent<Collider2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();

        SetVisible(visible);

        startPos = transform.position;
        desiredPos1 = transform.position + yMovement;
    }

    protected virtual void EndBlock()
    {

    }

    protected virtual IEnumerator GetBonus ()
    {
        yield return new WaitForEndOfFrame();
    }

    /// <summary>
    /// Bonus types translations temp function. Pending to merge all kinds of bonus.
    /// </summary>
    /// <param name="iBonusType"></param>
    /// <returns></returns>
    protected virtual ScoreBonus.ScoreBonusType GetScoreEffectTranslation (Bonus iBonusType)
    {
        ScoreBonus.ScoreBonusType scoreBonusType = ScoreBonus.ScoreBonusType.EmptyBlock;

        switch (iBonusType)
        {
            case Bonus.Coins:
                scoreBonusType = ScoreBonus.ScoreBonusType.Coin;
                break;
            case Bonus.Empty:
                scoreBonusType = ScoreBonus.ScoreBonusType.EmptyBlock;
                break;
            case Bonus.Flower:
                scoreBonusType = ScoreBonus.ScoreBonusType.Flower;
                break;
            case Bonus.Live:
                //scoreBonusType = ScoreBonus.ScoreBonusType.
                break;
            case Bonus.Mushroom:
                scoreBonusType = ScoreBonus.ScoreBonusType.Mushroom;
                break;
            case Bonus.Nothing:
                scoreBonusType = ScoreBonus.ScoreBonusType.EmptyBlock;
                break;
            case Bonus.Star:
                break;

        }
        return scoreBonusType;
    }

    /// <summary>
    /// Report bonus type and later effect target position
    /// </summary>
    /// <param name="bonus"></param>
    /// <param name="position"></param>
    protected virtual void ReportBonus (ScoreBonus.ScoreBonusType bonus, Vector3 position)
    {
        GlobalRefs.currentStageManager.ReportBonus(bonus, transform.position);
    }

    protected virtual void SetVisible (bool vis)
    {
        spriteRenderer.enabled = vis;
    }

    /// <summary>
    /// Block shake effect when player touchs it
    /// </summary>
    /// <returns></returns>
    IEnumerator Shake ()
    {
        float t = 0f;

        while (transform.position.y < desiredPos1.y)
        {
            transform.position = Vector3.Lerp(startPos, startPos + yMovement, t);
            t += (movementSpeed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }

        StartCoroutine(GetBonus());

        t = 0f;

        while (transform.position.y > startPos.y)
        {
            transform.position = Vector3.Lerp(startPos + yMovement, startPos, t);
            t += (movementSpeed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }

        blockCollider.enabled = true;
    }

    public void Touched ()
    {
        blockCollider.enabled = false;
        StartCoroutine(Shake());
    }
}

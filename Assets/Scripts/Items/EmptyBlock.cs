﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Empty block (wall bricks version). Pending to add possible coin bonus feature
/// </summary>
public class EmptyBlock : InteractibleBlock
{
    // Breaking wall bricks block particle system
    public GameObject explosionEffect;

    protected override void Start()
    {
        base.Start();
    }

    protected override void EndBlock()
    {
        Instantiate(explosionEffect, transform.position, Quaternion.identity);

        base.EndBlock();

        Destroy(gameObject);
    }

    protected override IEnumerator GetBonus()
    {
        if (GlobalRefs.currentStageManager.GetPlayerBehavior().superMario)
        {
            ReportBonus(ScoreBonus.ScoreBonusType.EmptyBlock, transform.position);
            EndBlock();
        }

        yield return new WaitForEndOfFrame();
    }
}

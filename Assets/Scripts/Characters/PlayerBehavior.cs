﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Human player behavior
/// </summary>
public class PlayerBehavior : MonoBehaviour
{
    // Two colliders (one for usual state and one for "super" state)
    public Collider2D marioCollider;
    public Collider2D superMarioCollider;

    // For disabling damaging events pause.
    public float noDamageTime = 2.0f;

    // For mario dead animation time purposes.
    public float marioDeathMotionTime = 2.0f;
    public float marioDeathMotionVelocityMultiplier = 2.0f;

    // For player motion customization.
    public float playerVelocityMultiplier;
    public float playerJumpingMultiplier;

    // All "ground" layers mask.
    public LayerMask groundMask;

    public LayerMask destructableBlocksMask;

    public enum MotionState { Idle, Walking, Jumping, Falling, Paused, Dead}
    public MotionState motionState;

    private bool lookingLeft; // Player's 2D direction

    private float distToGround; // Player's distance to ground

    /// <summary>
    /// "Super" state. Pending to convert to a more protected type.
    /// </summary>
    public bool superMario;

    // Is Mario touching the ground?
    private bool onGround;

    private Rigidbody2D playerRigidbody;

    private Animator playerAnimator;

    private SpriteRenderer playerRenderer;

    private Collider2D playerCollider;

    private float horizontalAxis;
    private bool jumpButton;

    private RaycastHit2D[] headHits;

    private bool noDamagePause = false;

    /// <summary>
    /// To avoid more than one interactable block touch
    /// </summary>
    private bool oneObjectHeaded;

    // Use this for initialization
    void Start ()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        playerAnimator = GetComponent<Animator>();
        playerRenderer = GetComponent<SpriteRenderer>();
        playerCollider = GetComponent<Collider2D>();

        InitPlayerProps();
	}
	
    void Animate ()
    {
        playerAnimator.SetFloat("velocity", Mathf.Abs(playerRigidbody.velocity.x));
        playerAnimator.SetBool("jumping", motionState == MotionState.Jumping);
        playerAnimator.SetBool("super", superMario);
        playerAnimator.SetBool("dead", motionState == MotionState.Dead);
        playerRenderer.flipX = lookingLeft;
    }

    /// <summary>
    /// If in "Super" state then disable it and start a no damage pause to avoiding almost same time damage events.
    /// </summary>
    void DamagePlayer ()
    {
        GlobalRefs.currentSoundManager.PlaySound(SoundManager.Sounds.Damage);

        if (superMario)
        {
            StartCoroutine(NoDamagePauseTimer());
            SetSuperMario(false);
        }
        else
        {
            DeadPlayer();
        }
    }

    /// <summary>
    /// Deactivate physics and start player dead animation.
    /// </summary>
    public void DeadPlayer ()
    {
        GlobalRefs.currentGameManager.player.currentLives--;
        PausePlayer(true);
        playerRigidbody.velocity = Vector2.zero;
        playerRigidbody.bodyType = RigidbodyType2D.Kinematic;
        GlobalRefs.currentStageManager.PlayerDead();
        SetMotionState(MotionState.Dead);

        StartCoroutine(DeadMotion());
    }

    /// <summary>
    /// Up and down motion animation at player dead time.
    /// </summary>
    /// <returns></returns>
    IEnumerator DeadMotion()
    {
        float startTime = Time.time;

        DisablePlayerColliders();
        playerRigidbody.freezeRotation = false;

        while (Time.time < (startTime + marioDeathMotionTime))
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + (marioDeathMotionVelocityMultiplier * Time.deltaTime), transform.position.z);
            yield return new WaitForEndOfFrame();
        }

        startTime = Time.time;

        while (Time.time < (startTime + marioDeathMotionTime))
        {
            transform.position = new Vector3(transform.position.x, transform.position.y - (marioDeathMotionVelocityMultiplier * 2 * Time.deltaTime), transform.position.z);
            yield return new WaitForEndOfFrame();
        }

        if (GlobalRefs.currentGameManager.player.currentLives < 1)
        {
            GlobalRefs.currentStageManager.SetMatchState(StageManager.MatchState.Lost);
        }
        else
        {
            GlobalRefs.currentStageManager.SetMatchState(StageManager.MatchState.Reseting);
        }

        playerRigidbody.freezeRotation = true;
    }

    void DisablePlayerColliders ()
    {
        marioCollider.enabled = false;
        superMarioCollider.enabled = false;
    }

    /// <summary>
    /// Multiplatform target. Android version could be a good idea.
    /// </summary>
    void GetInput () // Conditional compilation for the input method
    {
#if UNITY_EDITOR
        horizontalAxis = Input.GetAxis("Horizontal");
        jumpButton = Input.GetButtonDown("Jump");

#elif UNITY_STANDALONE_WIN
        Debug.Log("Unity iPhone");

#elif UNITY_ANDROID
        Debug.Log("Unity iPhone");

#else
        Debug.Log("Any other platform");
#endif

    }

    /// <summary>
    /// Player arrives to the end of the stage.
    /// </summary>
    public void GoalAchieved ()
    {
        ShowPlayer(false);
        SetMotionState(MotionState.Paused);
        // TODO : score recount
        GlobalRefs.currentStageManager.SetMatchState(StageManager.MatchState.Won);
    }

    void InitPlayerProps ()
    {
        SetMarioCollider(true);

        onGround = false;

        SetMotionState(MotionState.Idle);

        distToGround = playerCollider.bounds.extents.y;
    }

    bool IsGrounded()
    {
        return Physics2D.Raycast(transform.position, -Vector2.up, distToGround + 0.01f, groundMask);
    }

    void Jump ()
    {
        PlayJumpSound();
        playerRigidbody.AddForce(new Vector2(0f, playerJumpingMultiplier), ForceMode2D.Impulse);
    }

    /// <summary>
    /// Alternative jump method for manual calls.
    /// </summary>
    /// <param name="jumpForce"></param>
    void Jump (float jumpForce)
    {
        playerRigidbody.AddForce(new Vector2(0f, playerJumpingMultiplier * jumpForce), ForceMode2D.Impulse);
    }

    /// <summary>
    /// It manages the player motion and state machine.
    /// </summary>
    void Move ()
    {
        playerRigidbody.velocity = new Vector3(horizontalAxis * playerVelocityMultiplier, playerRigidbody.velocity.y, 0f);

        if (onGround)
        {
            if (playerRigidbody.velocity.x > 0f)
            {
                lookingLeft = false;
                SetMotionState(MotionState.Walking);
            }
            else if (playerRigidbody.velocity.x < 0f)
            {
                lookingLeft = true;
                SetMotionState(MotionState.Walking);
            }
            else
            {
                 SetMotionState(MotionState.Idle);
            }

            if (jumpButton)
            {
                Jump();
            }
        }
        else
        {
            SetMotionState(MotionState.Jumping);
        }
    }

    /// <summary>
    /// Centralized game objects interactuation to avoid sensors coding dispersion and a better optimization. ("thinking too much" time) 
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter2D (Collision2D collision)
    {
        if ((motionState != MotionState.Paused) && (motionState != MotionState.Dead))
        {
            switch (collision.gameObject.layer)
            {
                case 13: // Interactible blocks
                    if (!oneObjectHeaded) /// To avoid more than one interactable block touch
                    {
                        if (collision.transform.position.y > transform.position.y)
                        {
                            oneObjectHeaded = true;
                            collision.gameObject.SendMessage("Touched", SendMessageOptions.RequireReceiver);
                        }
                    }
                    break;
                case 14: // Items
                    // TODO : hashable tag?
                    if (collision.gameObject.tag == "Mushroom")
                    {
                        collision.gameObject.SendMessage("EndMushroom", SendMessageOptions.RequireReceiver);

                        if (superMario)
                        {
                            // TODO : pending to add other mario states and their behaviors
                            GlobalRefs.currentStageManager.AchieveReMushroom(collision.transform.position);
                        }
                        else
                        {
                            GlobalRefs.currentStageManager.AchieveMushroom(transform.position);
                            SetSuperMario(true);
                        }
                    }
                    break;
                case 17: // Enemies
                    if (transform.position.y > collision.collider.bounds.center.y)
                    {
                        collision.gameObject.SendMessage("DamageEnemy", SendMessageOptions.RequireReceiver);
                        Jump(0.5f);
                    }
                    else
                    {
                        if (!noDamagePause)
                        {
                            DamagePlayer();
                        }
                    }
                    break;
            }
        }
    }

    /// <summary>
    /// Pause moment for player post-damaged times. Avoids re-damage.
    /// </summary>
    /// <returns></returns>
    IEnumerator NoDamagePauseTimer ()
    {
        noDamagePause = true;
        yield return new WaitForSeconds(noDamageTime);

        noDamagePause = false;
    }

    void PausePlayer (bool pause)
    {
        if (pause)
        {
            SetMotionState(MotionState.Paused);
        }
        else
        {
            SetMotionState(MotionState.Idle);
        }
    }

    void PlayJumpSound ()
    {
        GlobalRefs.currentSoundManager.PlaySound(SoundManager.Sounds.Jump);
    }

    /// <summary>
    /// Reset player position when player deads.
    /// </summary>
    public void ResetPlayer ()
    {
        transform.position = GlobalRefs.currentStageManager.spawnPoints[0].transform.position;
        PausePlayer(false);
        playerRigidbody.velocity = Vector2.zero;
        marioCollider.enabled = true;
        playerRigidbody.bodyType = RigidbodyType2D.Dynamic;
    }

    /// <summary>
    /// Toggle between usual mario and "super" mario colliders.
    /// </summary>
    /// <param name="marioCol"></param>
    void SetMarioCollider (bool marioCol)
    {
        marioCollider.enabled = marioCol;
        superMarioCollider.enabled = !marioCol;
    }

    /// <summary>
    /// Player animation and state machine
    /// </summary>
    /// <param name="ms"></param>
    void SetMotionState (MotionState ms)
    {
        motionState = ms;
        Animate();
    }

    /// <summary>
    /// Set "super" state. Pending to change superMario property protection
    /// </summary>
    /// <param name="setIt"></param>
    public void SetSuperMario (bool setIt)
    {
        superMario = setIt;
        playerAnimator.SetBool("super", setIt);
        SetMarioCollider(!setIt);
    }

    private void ShowPlayer (bool show)
    {
        playerRenderer.enabled = show;
    }

    private void FixedUpdate()
    {
        onGround = IsGrounded();

        /// To avoid more than one interactable block touch
        if (onGround && oneObjectHeaded)
        {
            oneObjectHeaded = false;
        }
    }

    void Update ()
    {
        GetInput();

        if ((motionState != MotionState.Paused) && (motionState != MotionState.Dead))
        {
            Move();
        }
    }
}

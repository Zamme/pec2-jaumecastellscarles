﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Stage goal sensor. Used for end-of-stage time.
/// </summary>
public class StageGoal : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.SendMessage("GoalAchieved", SendMessageOptions.RequireReceiver);
        }
    }
}

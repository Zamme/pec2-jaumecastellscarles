﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenusBehavior : MonoBehaviour
{
    public Text resultText;

    private void Start()
    {
        string result;

        if (GlobalRefs.currentGameManager.wonLastMatch)
        {
            result = "You Won";
        }
        else
        {
            result = "You Lost";
        }
        resultText.text = result;
    }
    public void Quit ()
    {
        Application.Quit();
    }

    public void Restart ()
    {
        SceneManager.LoadScene(0);
    }
}

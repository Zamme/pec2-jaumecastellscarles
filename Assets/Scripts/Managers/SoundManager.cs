﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public int nAudioSources; // Min = 2 to avoid music overriding

    public AudioClip jumpAudioClip;
    public AudioClip coinAudioClip;
    public AudioClip killAudioClip;
    public AudioClip superMarioAudioClip;
    public AudioClip breakBlockAudioClip;
    public AudioClip bumpAudioClip;
    public AudioClip powerUpAppearsAudioClip;

    public enum Sounds {Jump, Coin, Kill, SuperMario, BreakBlock, Damage, PowerUpAppears}

    private AudioSource[] audioSources;
    private int lastUsed = -1;
    private int iMusicAudioSource = -1;

    private void Awake()
    {
        GlobalRefs.currentSoundManager = this;

        InitAudioSources();
    }

    void Start ()
    {
        //DontDestroyOnLoad(gameObject);
	}
	
    AudioSource GetNextAudioSource ()
    {
        NextAudioSourceIndex();
        return audioSources[lastUsed];
    }

    void InitAudioSources ()
    {
        if (nAudioSources < 2)
        {
            Debug.LogError("Audio sources number must be 2 minimum!");
            return;
        }

        audioSources = new AudioSource[nAudioSources];

        for (int a = 0; a < nAudioSources; a++)
        {
            audioSources[a] = gameObject.AddComponent<AudioSource>();
        }
    }

    void NextAudioSourceIndex ()
    {
        lastUsed++;

        if (lastUsed == audioSources.Length)
        {
            lastUsed = 0;
        }

        if (audioSources[lastUsed].isPlaying)
        {
            NextAudioSourceIndex();
        }
    }

    public int PlayAudioClip (AudioClip audioClip)
    {
        AudioSource audioSource = GetNextAudioSource();
        audioSource.clip = audioClip;
        audioSource.Play();

        return lastUsed;


    }

    public void PlayMusic (AudioClip audioClip)
    {
        if (iMusicAudioSource < 0)
        {
            iMusicAudioSource = PlayAudioClip(audioClip);
        }
        else
        {
            audioSources[iMusicAudioSource].Play();
        }

        audioSources[iMusicAudioSource].loop = true;
    }

    public void PlaySound (Sounds sound)
    {
        switch (sound)
        {
            case Sounds.Coin:
                PlayAudioClip(coinAudioClip);
                break;
            case Sounds.Jump:
                PlayAudioClip(jumpAudioClip);
                break;
            case Sounds.Kill:
                PlayAudioClip(killAudioClip);
                break;
            case Sounds.SuperMario:
                PlayAudioClip(superMarioAudioClip);
                break;
            case Sounds.BreakBlock:
                PlayAudioClip(breakBlockAudioClip);
                break;
            case Sounds.Damage:
                PlayAudioClip(bumpAudioClip);
                break;
            case Sounds.PowerUpAppears:
                PlayAudioClip(powerUpAppearsAudioClip);
                break;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// GameManager manages from app loading to app end time
/// </summary>
public class GameManager : MonoBehaviour
{
    public GameObject soundManagerPrefab;
    public PlayerStats player;

    // Score bonus component ref.
    private ScoreBonus scoreBonus;

    private GameObject currentMainCamera;
    private CameraBehavior currentMainCameraBehavior;

    // TODO : pending to convert some public properties to more protected kinds of.
    public bool wonLastMatch;

    private void Awake()
    {
        GlobalRefs.currentGameManager = this;

        scoreBonus = GetComponent<ScoreBonus>();

        InitializePlayer();
    }

    void Start ()
    {
        GetCurrentMainCamera();

        CreateSoundManager();

        // Meanwhile, for the moment, first stage. Pending to extend level management.
        GlobalRefs.currentStageManager.StartStage();
	}
	
    /// <summary>
    /// The sound manager is created by the game manager if it doesn't exist
    /// </summary>
    void CreateSoundManager ()
    {
        Instantiate(soundManagerPrefab, Vector3.zero, Quaternion.identity);
    }

    public void GameOver (bool won)
    {
        SceneManager.LoadScene("GameOver");
    }

    void GetCurrentMainCamera ()
    {
        currentMainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        currentMainCameraBehavior = currentMainCamera.GetComponent<CameraBehavior>();
    }

    public CameraBehavior GetCurrentMainCameraBehavior ()
    {
        return currentMainCameraBehavior;
    }

    public GameObject GetCurrentMainCameraObject ()
    {
        return currentMainCamera;
    }

    /// <summary>
    /// Score bonus component reference getter.
    /// </summary>
    /// <returns></returns>
    public ScoreBonus GetScoreBonusRef ()
    {
        return scoreBonus;
    }

    /// <summary>
    /// Player stats to zero... except lives (pending to standarize at loading options feature time)
    /// </summary>
    void InitializePlayer ()
    {
        player = new PlayerStats(0, 0, 0, 0, 3); // TODO : Loading options
    }

    /// <summary>
    /// Load level function. Pending to add.
    /// </summary>
    //void LoadStage ()
    //{

    //}
}

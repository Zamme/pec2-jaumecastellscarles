﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private Text uiScore;
    private Text uiCoins;
    private Text uiLevel;
    private Text uiTime;

    private GameObject hud;

    private Text scoreText;
    private Text coinsText;
    private Text livesText;
    private Text levelText;

    private GameObject startPage;

    private void Awake()
    {
        GlobalRefs.currentUIManager = this;
        GetUIRefs();
    }

    void Start ()
    {
        GetComponent<Canvas>().worldCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }

    void GetUIRefs()
    {
        uiScore = GameObject.Find("ScoreText").GetComponent<Text>();
        uiCoins = GameObject.Find("CoinsText").GetComponent<Text>();
        uiLevel = GameObject.Find("LevelText").GetComponent<Text>();
        uiTime = GameObject.Find("TimeText").GetComponent<Text>();

        hud = GameObject.Find("HUD");

        scoreText = GameObject.Find("StartStageScoreText").GetComponent<Text>();
        coinsText = GameObject.Find("StartStageCoinsText").GetComponent<Text>();
        livesText = GameObject.Find("StartStageLivesText").GetComponent<Text>();
        levelText = GameObject.Find("StartStageLevelText").GetComponent<Text>();

        startPage = GameObject.Find("StartStageUI");
    }

    public void ActivateHud ()
    {
        ShowHud(true);
        ShowStartPage(false);
    }

    public void ActivateStartPage ()
    {
        ShowHud(false);
        ShowStartPage(true);
        UpdateStartPage();
    }

    public IEnumerator PauseForSeconds(float seconds)
    {
        yield return new WaitForSeconds(seconds);

        GlobalRefs.currentStageManager.SetMatchState(StageManager.MatchState.Starting);

    }

    void ShowHud (bool show)
    {
        hud.SetActive(show);
    }

    void ShowStartPage (bool show)
    {
        startPage.SetActive(show);
    }

    void ToggleCanvas ()
    {
        ShowHud(!hud.activeSelf);
        ShowStartPage(!startPage.activeSelf);
    }

    public void UpdateHUD()
    {
        UpdateUIScore();
        UpdateUICoins();
        UpdateUILevel();
        UpdateUITime();
    }

    public void UpdateStartPage ()
    {
        scoreText.text = GlobalRefs.currentGameManager.player.currentStageScore.ToString("000000");
        coinsText.text = GlobalRefs.currentGameManager.player.currentStageCoins.ToString("00");
        levelText.text = (" " + GlobalRefs.currentStageManager.levelName);
        livesText.text = GlobalRefs.currentGameManager.player.currentLives.ToString();
    }

    public void UpdateUICoins()
    {
        uiCoins.text = GlobalRefs.currentGameManager.player.currentStageCoins.ToString("00");
    }

    public void UpdateUILevel()
    {
        uiLevel.text = GlobalRefs.currentStageManager.levelName;
    }

    public void UpdateUIScore()
    {
        uiScore.text = GlobalRefs.currentGameManager.player.currentStageScore.ToString("000000");
    }

    public void UpdateUITime()
    {
        uiTime.text = GlobalRefs.currentStageManager.currentMatchTime.ToString("000");
    }

}

# UOC – Máster en diseño y desarrollo de Videojuegos 2018
# Jaume Castells Carles
# PEC 2 - Un juego de plataformas

## Nombre
**Mario Bros. Clone**

## Descripción
Clone del videojuego **Mario Bros.** (Nintendo)

## Arte
Todo el arte es propiedad de Nintendo.

## Builds
Builds available:
### Android
Default target
### Windows
Path Builds/Win
### Linux
### Mac

## Programación
El objeto principal es el **GameManager**. Se encarga de crear y mantener la partida del jugador (estadísticas y info), cambiar de nivel, y de crear el manager de sonido además de otros cometidos generales.

Además del manager de juego también existen otros con sus propios cometidos:
* **StageManager**; se encarga de crear y mantener la pantalla actual. Está pensado para usarse de modo local en cada escena (nivel) con sus propiedades. Hay tantos StageManager como niveles.
* **SoundManager**; encargado del sonido. Sólo hay uno por partida ya que todo el tema sonido está centralizado en él. Contiene varias fuentes de sonido que se encarga de administrar.
* **UIManager**; encargado de la interfaz de usuario. Cuida tanto de los menús como del hud del jugador.

### Otras clases importantes
#### PlayerBehavior
Clase encargada del funcionamiento del player humano.

#### CameraBehavior
Clase base abstracta para el desarrollo de las camaras de juego.

De momento sólo hay una, la SMWCamera (Super Mario World cam).

#### SMWCamera
Sistema de cámara con las peculiaridades y el comportamiento de la cámara del videojuego para NES Mario Bros.

#### InteractibleBlock
Clase base para el comportamiento de los "blocks" interactivos (con sorpresa o no en forma de bonus en el interior).

#### QuestionBlock
Clase heredada de InteractibleBlock para los blocks interrogante (?).

**Mucha más información en el código de los scripts en forma de comentarios y sumarios.**

## Para mejorar en futuras versiones
Hay muchos detalles que no he tenido tiempo de añadir y/o arreglar. Espero hacerlo en futuras versiones:  :-)
* Enemigos Koopa.
* Bonus como la flor y la estrella.
* Algunas animaciones de Mario.
* Algunos efectos de sonido
* Algunos estados de Mario (p.e. disparo).
 
